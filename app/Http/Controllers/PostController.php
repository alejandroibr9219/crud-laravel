<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Post;

class PostController extends Controller
{
       public function __construct()
    {
        $this->middleware('auth');
    }

     public function create(){
        
        return view('post.create');
    }
    public function save(Request $request){
        
              $validate = $this->validate($request, [
              'title'  => 'required',
              'content' => 'required',
          
               ]);
        


         
              $content = $request->input('content'); 
              $title = $request->input('title');   

              $user = \Auth::user();
              $post = new Post();
              $post->user_id = $user->id;
              $post->content = $content;
              $post->title = $title;

           
          $post->save();
          return redirect()->route('home')->with([
              
              'message' => 'La publicacion ha sido subida Correctamente!!'
          ]);
              
    }
 
    public function detail($id){
       $posts = Post::find($id);
       return view('post.detail', [
           'post' =>$posts
       ]);
    }
    
}
