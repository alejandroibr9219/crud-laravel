<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Comment;



class CommentController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

     public function save(Request $request){
        $validate = $this->validate($request,[
            'post_id' => 'integer|required',
            'content' => 'string|required',

       ]);
        $user = \Auth::user();
        $post_id = $request->input('post_id');
        $content = $request->input('content');

        $isset_comment = Comment::where('user_id', $user->id)
                                ->where('post_id', $post_id)
                                ->count();
            
        if($isset_comment == 0){
      
        $comment = new Comment();
        $comment -> user_id = $user->id;
        $comment -> post_id= $post_id;
        $comment -> content = $content;
        $comment-> status = 'Aprobado';
        

        $comment ->save();
        
        $data = array (
                'comment' => $comment,
            );
            Mail::send ('emails.comment', $data, function ($message){

                $message->from('meyeralex357@gmail.com', 'CRUD LARAVEL');
                $message->to('meyeralex357@gmail.com')->subject('Hay un nuevo comentario!!');
            });
        return redirect()->route('post.detail', ['id' => $post_id])
                -> with([
                    'message' => 'Has publicado tu comentario Correctamente!!'
                ]);
       }else{
       return redirect()->route('post.detail', ['id' => $post_id])
                -> with([
                    'message' => 'Ya comentaste esta publicacion!!'
                ]);
         
              
       }
    }
   

}
