<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test-orm', 'PruebasController@testORM');
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/subir-post', 'PostController@create')->name('post.create');
Route::post('/post/save', 'PostController@save')->name('post.save');
Route::get('/image/file/{filename}', 'PostController@getImage')->name('post.file');
Route::get('/post/{id}', 'PostController@detail')->name('post.detail');

Route::post('/comment/save', 'CommentController@save')->name('comment.save');



