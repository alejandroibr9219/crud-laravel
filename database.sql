CREATE DATABASE IF NOT EXISTS crud_aw;
USE crud_aw;

CREATE TABLE IF NOT EXISTS users(
id              int(255) auto_increment not null,
name            varchar(100),
surname         varchar(200),
email           varchar(255),
password        varchar(255),
created_at      datetime,
updated_at      datetime,
remember_token  varchar(255),
CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;


INSERT INTO users VALUES(NULL,  'Ivanr', 'Briceño',  'ivanr@ivan.com', 'pass',  CURTIME(), CURTIME(), NULL);
INSERT INTO users VALUES(NULL,  'Juan', 'Lopez',  'juan@juan.com', 'pass',  CURTIME(), CURTIME(), NULL);
INSERT INTO users VALUES(NULL,  'Manolo', 'Garcia',  'manolo@manolo.com', 'pass', CURTIME(), CURTIME(), NULL);

CREATE TABLE IF NOT EXISTS publications(
id              int(255) auto_increment not null,
user_id         int(255),
title           text,
content         text,
created_at      datetime,
updated_at      datetime,
CONSTRAINT pk_publications PRIMARY KEY(id),
CONSTRAINT fk_publications_users FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;

INSERT INTO publications VALUES(NULL, 1, 'hola', 'descripción de prueba 1', CURTIME(), CURTIME());
INSERT INTO publications VALUES(NULL, 1,'hola', 'descripción de prueba 2', CURTIME(), CURTIME());
INSERT INTO publications VALUES(NULL, 1,'hola', 'descripción de prueba 3', CURTIME(), CURTIME());
INSERT INTO publications VALUES(NULL, 3,'hola',  'descripción de prueba 4', CURTIME(), CURTIME());


CREATE TABLE IF NOT EXISTS comments(
id              int(255) auto_increment not null,
post_id			int(255),
user_id         int(255),
content         text,
status			text,
created_at      datetime,
updated_at      datetime,
CONSTRAINT pk_comments PRIMARY KEY(id),
CONSTRAINT fk_comments_post_id FOREIGN KEY(post_id) REFERENCES publications(id),
CONSTRAINT fk_comments_users FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;

INSERT INTO comments VALUES(NULL, 1, 2, 'Buena foto de familia!!','APROBADO', CURTIME(), CURTIME());
INSERT INTO comments VALUES(NULL, 1,2, 'Buena foto de PLAYA!!','APROBADO', CURTIME(), CURTIME());
INSERT INTO comments VALUES(NULL, 3,1, 'que bueno!!','APROBADO', CURTIME(), CURTIME());

