@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('includes.message')
            @foreach($posts as $post)
        <div class="card pub_image">    
           <div class="card-header">

               <div class="data-user">

                {{$post->user->name.' '.$post->user->surname}}
               
            </div>
              
                   </div> 
               <div class="card-body">
                    <div class="title">
                      <a href="{{ route('post.detail', ['id' => $post->id])}}">
                     <h1>{{ $post->title}}</h1>
                   </div>
                 </a>
                   <hr>
                   <div class="content">
                     {{ $post->content}}
                   </div>
                    <div class="comments">
                    <a href="" class="btn btn-sm btn-warning btn-comments">
                      Comentarios ({{count($post->comments)}})
                    </a>
                  </div>

                
               </div>

           </div>

            @endforeach
                <!-- PAGINACION -->

             <div class="clearfix"></div>
                {{$posts->links()}}
            
        </div>

      </div>
    </div>
@endsection
