@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @include('includes.message')
          
        <div class="card pub_image pub_image_detail">    
           <div class="card-header">

               <div class="data-user">

                {{$post->user->name.' '.$post->user->surname}}
               
            </div>
              
                   </div> 
               <div class="card-body">
                 
                    <div class="title">
                     <h1>{{ $post->title}}</h1>
                   </div>
                   <hr>
                   <div class="content">
                     <h4>{{ $post->content}}</h4>
                   </div>
                   <div class="clearfix"></div>
                     <div class="comments">
                   
                     <h2> Comentarios ({{count($post->comments)}})</h2>
                     <hr>

                     <form method="POST" action="{{ route ('comment.save') }}">
                            @csrf
                            <input type="hidden" name="post_id" value="{{$post->id}}"/>
                            <p>
                                <textarea class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}" name="content" ></textarea>
                                    @if($errors->has('content'))
                            <span class="invalid-feedback" role="alert">
                                <strong> {{ $errors->first('content') }} </strong>
                            </span>
                            @endif
                            </p>
                            <button type="submit" class="btn btn-success">Enviar</button>
                        </form>
                        <hr>
                           @foreach($post->comments as $comment)
                      <div class="comment">
              
                       <span class="nickname"><h1>{{$comment->user->name}}</h1></span>
                        
                        <p>{{$comment->content}}<hr>
                        @endforeach
                  </div>
               </div>

           </div>

     
            
            
        </div>

      </div>
    </div>
@endsection
