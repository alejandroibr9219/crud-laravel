@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">Subir Nueva Publicacion</div>

                <div class="card-body">
                    <form method="POST" action=" {{ route('post.save') }}" enctype="multipart/form-data">
                        @csrf

                   

                          <div class="form-group row">
                            <label for="title" class="col-md-3 col-form-label text-md-right">Titulo</label>
                            <div class="col-md-7">
                                <textarea id="title" name="title" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" required> </textarea>  
                            @if($errors->has('title'))
                            <span class="invalid-freedback" role="alert">
                                <strong> {{ $errors->first('title') }} </strong>
                            </span>
                            @endif
                            </div>
                        </div>
                        
                        
                             <div class="form-group row">
                            <label for="content" class="col-md-3 col-form-label text-md-right">Contenido</label>
                            <div class="col-md-7">
                                <textarea id="content" name="content" class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}" required> </textarea>  
                            @if($errors->has('content'))
                            <span class="invalid-freedback" role="alert">
                                <strong> {{ $errors->first('content') }} </strong>
                            </span>
                            @endif
                            </div>
                        </div>
                        
                                    <div class="form-group row">
                          
                            <div class="col-md-6 offset-md-3">
                                <input  type="submit" class="btn btn-primary" value="Subir Publicacion"/>
                          
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection


